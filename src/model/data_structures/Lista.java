package model.data_structures;

public class Lista<T> implements ILista {
	
	private Nodo<T> ultimo;
	private Nodo<T> primero;
	private int size;
	
	public Lista() 
	{
		size = 0;
	}

	@Override
	public int compareTo(Object arg0) {
		return 0;
	}

	@Override
	public void add(Object elem) {
		if (!isEmpty()) {
			Nodo<T> nodo = new Nodo<T>(elem, null, ultimo);
			ultimo.cambiarSiguiente(nodo);
			ultimo = nodo;	
			size++;		
		}
		else
		{
			Nodo<T> nodo = new Nodo<T>(elem, null, null);
			primero = nodo;
			ultimo = nodo;
			size++;
		}	
	}

	@Override
	public void remove(Object elem) {
		boolean termino = false;
		for (Nodo<T> n = primero; n.darSiguiente() != null && !termino; n.darSiguiente()) {
			if (n.darElem().equals(elem)) {
				n.darAnterior().cambiarSiguiente(n.darSiguiente());
				n.darSiguiente().cambiarAnterior(n.darAnterior());
				termino=true;
				size--;
			}
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Nodo<T> get(Object elem) {
		boolean termino = false;
		Nodo<T> nodo = null;
		for (Nodo<T> n = primero; n.darSiguiente() != null && !termino; n.darSiguiente()) {
			if (n.darElem().equals(elem)) {
				nodo= n;
				termino = true;
			}
		}
		return nodo;
	}

	@Override
	public Object get(int pos) {
		boolean termino = false;
		int i=0;
		for (Nodo<T> n = primero; i<=pos && !termino; n.darSiguiente())
		{
			if (i==pos) 
			{
				return n;
			}
		}
		return null;
	}

	@Override
	public boolean isEmpty() {
		if (size==0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Nodo<T> darPrimero()
	{
		return primero;
	}
	
	public Nodo<T> darUltimo()
	{
		return ultimo;
	}

}
