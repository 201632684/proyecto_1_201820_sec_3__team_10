package model.data_structures;

import java.util.Iterator;

public class Pila<T> implements IPila
{
	private Lista<T> lista;
	
	public Pila()
	{
		lista = new Lista<>();
	}
	
	@Override
	public void push(Object elem) {
		// TODO Auto-generated method stub
		lista.add(elem);
	}

	@Override
	public Object pop() {
		// TODO Auto-generated method stub
		Object o = lista.darUltimo().darElem();
		lista.remove(lista.darUltimo());
		return o;
	}

	@Override
	public int getSize() {
		return lista.size();
	}

	@Override
	public boolean isEmpty() {
		return lista.isEmpty();
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
