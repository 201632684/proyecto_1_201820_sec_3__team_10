package model.logic;

import API.IManager;
import model.data_structures.Cola;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Lista;
import model.data_structures.Nodo;
import model.data_structures.Pila;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.management.timer.TimerMBean;

import com.sun.org.apache.bcel.internal.generic.NEW;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1";
	
	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2";
		
	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3";
			
	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4";
		
	//Ruta del archivo de trips 2017-Q1-Q4
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4";
	
	private Lista<Trip> listaTrips;
	
	private Lista<Station> listaStation;
	
	private Lista<Bike> listaBikes;
	
	public Manager()
	{
		//TODO
	}
	
	public Cola<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		Cola<Trip> cola = new Cola<Trip>();
		listaTrips = ordenarCronologicamenteHoraInicial(listaTrips);
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
				cola.enqueue(t);
			}
		}
		return cola;
    }

    public Lista<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        Lista<Bike> l = new Lista<Bike>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
				Bike b = getBikeByID(t.getBikeId());
				if (l.get(b) == null) {
					l.add(b);
				}
			}
			l = ordenarMayorAMenorViajesRealizados(l);
		}
    	return l;
    }

    public Lista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
    	Lista<Trip> lista = new Lista<Trip>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (bikeId == t.getBikeId()) {
				if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
					lista.add(t);
				}
			}
		}
		lista = ordenarCronologicamenteHoraInicial(lista);
    	
    	return lista;
    }

    public Lista<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
    	Lista<Trip> lista = new Lista<Trip>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (endStationId == t.getEndStationId()) {
				if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
					lista.add(t);
				}
			}
		}
		lista = ordenarCronologicamenteHoraFinal(lista);
    	
    	return lista;
    }

    public Cola<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {
        Cola<Station> cola = new Cola<>();
        for (Nodo<Station> n = listaStation.darPrimero(); n!=null; n.darSiguiente()) {
			Station s = (Station)n.darElem();
			if (s.getStartDate().compareTo(fechaComienzo)>=0) {
				cola.enqueue(s);
			}
		}
    	
    	return cola;
    }

    public Lista<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
    	Lista<Bike> l = new Lista<Bike>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
				Bike b = getBikeByID(t.getBikeId());
				if (l.get(b) == null) {
					l.add(b);
				}
			}
		}
    	l = ordenarMayorAMenorDistanciaRecorrida(l);
    	return l;
    }

    public Lista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
    	Lista<Trip> lista = new Lista<Trip>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (t.getBikeId()==bikeId) 
			{
				if (t.getTripDuration() < tiempoMaximo) 
				{
					if (t.getGender().equals(genero)) {
						lista.add(t);
					}
				}
			}
		}
		lista = ordenarCronologicamenteHoraInicial(lista);
    	
    	return lista;
    }

    public Lista<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
    	Lista<Trip> lista = new Lista<Trip>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (startStationId == t.getStartStationId()) {
				if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
					lista.add(t);
				}
			}
		}
		lista = ordenarCronologicamenteHoraInicial(lista);
    	
    	return lista;
        
    }

	public void C1cargar(String rutaTrips, String rutaStations) 
	{
		//TODO
	}
	
	public Cola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Pila<Trip> pila = new Pila<Trip>();
		Lista<Trip> lista = new Lista<Trip>();
		Cola<Trip> cola = new Cola<Trip>();
		listaTrips = ordenarCronologicamenteHoraInicial(listaTrips);
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
				if (t.getBikeId()==bikeId) {
					lista.add(t);
				}
			}
		}
		int endId=0;
		for (Nodo<Trip> n = lista.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (pila.isEmpty()) {
				pila.push(t);
				endId=t.getEndStationId();
			}
			else
			{
				if (endId==t.getStartStationId()) {
					pila.push(t);
					endId=t.getEndStationId();
				}
				else
				{
					cola.enqueue(pila.pop());
					cola.enqueue(t);
					endId = t.getEndStationId();
					pila = new Pila<>();
				}
			}
		}
		return cola;
	}

	public Lista<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		int x = 0;  //TODO
		Lista<Bike> l = new Lista<Bike>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (topBicicletas==x) {
				Bike b = getBikeByID(t.getBikeId());
				if (l.get(b) == null) {
					l.add(b);
				}
			}
			l = ordenarMayorAMenorDuracionViajes(l);
		}
		
		return l;
	}

	public Lista<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista<Trip> lista = new Lista<Trip>();
		Lista<Trip> listaInicio = new Lista<Trip>();
		Lista<Trip> listaFin = new Lista<Trip>();
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (StationId == t.getStartStationId()) {
				if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
					listaInicio.add(t);
				}
			}
		}
		listaInicio = ordenarCronologicamenteHoraInicial(listaInicio);
		for (Nodo<Trip> n = listaTrips.darPrimero(); n!=null; n.darSiguiente()) 
		{
			Trip t= (Trip) n.darElem();
			if (StationId == t.getEndStationId()) {
				if (t.getStartTime().compareTo(fechaInicial)>=0 && t.getStopTime().compareTo(fechaFinal) <= 0) {
					listaFin.add(t);
				}
			}
		}
		listaFin = ordenarCronologicamenteHoraFinal(listaFin);
		lista.add(listaInicio);
		lista.add(listaFin);
		
		
		return lista;
	}
	
	public Bike getBikeByID(int pBikeID)
	{
		for (Nodo<Bike> n = listaBikes.darPrimero(); n != null; n.darSiguiente()) {
			Bike b = (Bike) n.darElem();
			if (b.getBikeId() == pBikeID) {
				return b;
			}
		}
		return null;
	}
	
	public Lista<Trip> ordenarCronologicamenteHoraInicial(Lista<Trip> pLista)
	{
		Comparable[] c = new Comparable;
		for (Nodo<Trip> n = pLista.darPrimero(); n!= null; n.darSiguiente()) {
			Trip t = (Trip)n.darElem();
			LocalDateTime date = t.getStartTime();
			
		}
		Quick.sort(c);
	}
	
	public Lista<Trip> ordenarCronologicamenteHoraFinal(Lista<Trip> pLista)
	{
		Lista l = new Lista<>();
		//TODO ordenar lista
		
		return null;
	}
	
	public Lista<Bike> ordenarMayorAMenorViajesRealizados(Lista<Bike> pLista)
	{
		Lista l = new Lista<>();
		//TODO ordenar lista
		
		return null;
	}
	
	public Lista<Bike> ordenarMayorAMenorDistanciaRecorrida(Lista<Bike> pLista)
	{
		Lista l = new Lista<>();
		//TODO ordenar lista
		
		return null;
	}
	
	public Lista<Bike> ordenarMayorAMenorDuracionViajes(Lista<Bike> pLista)
	{
		Lista l = new Lista<>();
		//TODO ordenar lista
		
		return null;
	}
}
