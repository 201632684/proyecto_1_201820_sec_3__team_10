package model.data_structures;

public class Nodo<T> {
	private Nodo<T> siguiente;
	private Nodo<T> anterior;
	private Object elem;
	
	public Nodo(Object pElement, Nodo<T> pSiguiente, Nodo<T> pAnterior)
	{
		elem = pElement;
		siguiente = pSiguiente;
		anterior = pAnterior;
	}
	
	public Object darElem()
	{
		return elem;
	}
	
	public Nodo darSiguiente()
	{
		return siguiente;
	}
	public Nodo darAnterior()
	{
		return anterior;
	}
	public void cambiarSiguiente(Nodo<T> sig)
	{
		siguiente = sig;
	}
	public void cambiarAnterior(Nodo<T> ant)
	{
		anterior = ant;
	}
	public void modificarActual(T pElem)
	{
		elem = pElem;
	}
	
}
