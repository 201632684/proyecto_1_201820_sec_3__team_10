package model.logic;

public class Quick
{
	
	private static int partition(Comparable[] a, int lo, int hi) 
	{ 
	   int i = lo, j = hi+1; 
	   while (true) 
	   { 
	      while (less(a[i++], a[lo])) 
	         if (i == hi) break; 
	      while (less(a[lo], a[--j])) 
	         if (j == lo) break; 
	      if (i >= j) break; 
	      exch(a, i, j); 
	   } 
	   exch(a, lo, j); 
	   return j; 
	}

	private static void exch(Comparable[] a, int i, int j) {
		// TODO Auto-generated method stub
		Comparable c1= a[i];
		Comparable c2= a[j];
		a[i]=c2;
		a[j]=c1;
	}

	private static boolean less(Comparable comparable, Comparable comparable2) {
		// TODO Auto-generated method stub
		if (comparable.compareTo(comparable2)<0) 
		{
			return true;
		}
		return false;
	} 
	public static void sort(Comparable[] a) 
	{ 
	   StdRandom.shuffle(a); 
	   sort(a, 0, a.length - 1); 
	} 
	private static void sort(Comparable[] a, int lo, int hi) 
	   { 
	      if (hi <= lo) return; 
	      int j = partition(a, lo, hi); 
	      sort(a, lo, j-1); 
	      sort(a, j+1, hi); 
	  } 
	
}
