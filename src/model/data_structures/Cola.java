package model.data_structures;

import java.util.Iterator;

public class Cola<T> implements ICola{

	private Lista<T> lista;
	
	public Cola() {
		lista = new Lista<T>();
	}

	@Override
	public int getSize() {
		return lista.size();
	}

	@Override
	public boolean isEmpty() {
		return lista.isEmpty();
	}

	@Override
	public Iterator iterator() {
		return null;
	}

	@Override
	public void enqueue(Object elem) {
		lista.add(new Nodo<>(elem, null, lista.darUltimo()));
	}

	@Override
	public void dequeue(Object elem) {
		lista.remove(elem);
	}
}
